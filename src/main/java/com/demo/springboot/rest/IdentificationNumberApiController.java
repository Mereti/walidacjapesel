package com.demo.springboot.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class IdentificationNumberApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(IdentificationNumberApiController.class);
    @Autowired
    private CheckPESEL checkPESEL;


    @CrossOrigin
    @GetMapping(value = "/check-identification-number")
    public ResponseEntity<String> checkPESEL(@RequestParam(defaultValue = "") String id) {
        LOGGER.info("--- check identification number: {}", id);


        if(checkPESEL.checkPESEL(id)) {
            return new ResponseEntity<>(HttpStatus.OK);
        }else
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


}
