package com.demo.springboot;

import org.springframework.stereotype.Service;

@Service
public class CheckPESEL {

    private byte PESEL[] = new byte[11];
    private byte numbers[] = {1,3,7,9,1,3,7,9,1,3};

    public boolean checkPESEL(String pesel){
        if(pesel.length() == 11){
            for(int i =0; i < pesel.length();i++){
                PESEL[i] = Byte.parseByte(pesel.substring(i,i++));
            }
            if(checkPeselD()){
                return true;
            }

        }else return false;
        return false;
    }
    private boolean checkPeselD(){
        int sum = 0;
        for(int i=0;i<PESEL.length; i++){
            sum +=PESEL[i]*numbers[i];
        }
        sum %=10;
        sum = 10 - sum;
        sum %= 10;
        if(sum != PESEL[10]){
            return false;
        }else return true;
    }


}
//TODO: Sprawdzenie dugosci PESEL'a
//TODO: Zamiana na tablice byte string'a
//TODO: Napisanie metody odpowiadajacej sprawdzeniu poprawnosci pesela